require("./config/config"); //instantiate configuration variables
require("./global_functions"); //instantiate global functions
// require("./config/firebase_config"); //Firebase initialization

// require('custom-env').env()
const express = require('express');
// const logger = require('morgan');
// This will be our application entry. We'll setup our server here.
const http = require('http');
const passport = require("passport");
// var cors = require('cors')
// Set up the express app
const app = express();
// Log requests to the console.
// app.use(logger('dev'));
// app.use(cors())
// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(express.urlencoded({ extended: true })); //Parse URL-encoded bodies


// for parsing application/json
app.use(express.json());


//Passport
app.use(passport.initialize());

var routers = require('./routes/index');


//Import the Sequelize models. We import the whole folder, so Node will pull in index.js, which is where the connection to
//sequelize is made.
var models = require("./models");
//Sync Database
models.sequelize.sync({force: false}).then(function () {

  console.log('Nice! Database looks fine')

}).catch(function (err) {

  console.log(err, "Something went wrong with the Database Update!")

});
app.use('/', routers);

app.use("/", function (req, res) {
  res.statusCode = CONFIG.ERROR_CODE_API_NOT_FOUND; //send the appropriate status code
  res.json({ success: false, message: "API not found", data: {} });
});

// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//     var err = new Error("Not Found");
//     err.status = 404;
//     // next(err);
//     ReE(res, err, CONFIG.ERROR_CODE);
//   });

//   // error handler
//   app.use(function(err, req, res, next) {
//     // set locals, only providing error in development
//     res.locals.message = err.message;
//     res.locals.error = req.app.get("env") === "development" ? err : {};

//     // render the error page
//     res.status(err.status || 500);
//     // res.render("error");
//     ReE(res, err, CONFIG.ERROR_CODE);
//   });



const port = process.env.PORT || 8080;
app.set('port', port);
const server = http.createServer(app);
server.listen(port, function () {
  console.log('couseling api is listinig on port', port);
});


module.exports = app;