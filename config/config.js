CONFIG = {}
require('custom-env').env('dev')
//JWT
CONFIG.JWT_ENCRYPTION = 'arnkcompany'

//OTP
CONFIG.OTP_EXP_SECONDS = 1200 //2 minute

//API STATUS
CONFIG.SUCCESS_CODE = 200;
CONFIG.ERROR_CODE_SESSION_EXP = 440;
CONFIG.ERROR_CODE_UNAUTHORIZED = 401;
CONFIG.ERROR_CODE_API_NOT_FOUND = 414;


CONFIG.ERROR_CODE = 422;
CONFIG.ERROR_CODE_BLOCKED_USER = 450;
CONFIG.ERROR_CODE_DELETED_USER = 451;
CONFIG.ERROR_CODE_APP_UPDATE = 452;

//USER STATUS
CONFIG.USER_REGISTERED = 0;
CONFIG.USER_ACTIVE = 1;
CONFIG.USER_INACTIVE = 2;
CONFIG.USER_DELETED = 4;
CONFIG.USER_BLOCKED = 5;

//RECORD STATUS
CONFIG.RECORD_DELETED = 0;
CONFIG.RECORD_ACTIVE = 1;

//ADD USER
CONFIG.ERR_MISSING_EMAIL = "Please provide email";
CONFIG.ERR_MISSING_FNAME = "Please provide first name";
CONFIG.ERR_MISSING_LNAME = "Please provide last name";
CONFIG.ERR_MISSING_MNAME = "Please provide middle name";
CONFIG.ERR_MISSING_PHONE = "Please provide phone";
CONFIG.ERR_MISSING_AGE = "Please provide age";
CONFIG.ERR_MISSING_TYPE = "Please provide type";
CONFIG.ERR_MISSING_GENDER = "Please provide gender";
CONFIG.ERR_MISSING_PASS = "Please provide password";
CONFIG.ERR_USER_EXIST = "User is already exist";
CONFIG.ERR_SERVER = "Server not reachable";
CONFIG.SUCC_EMPLOYEE_ADDED = "Employee is added successfully";
CONFIG.ERR_USER_NOT_EXIST = "User is not exist";
CONFIG.ERR_MISSING_STATE = "Please provide state";
CONFIG.ERR_MISSING_CITY = "Please provide city";
CONFIG.ERR_MISSING_ADDRESS = "Please provide address";
CONFIG.ERR_MISSING_DOB = "Please provide birth date";
CONFIG.ERR_MISSING_DESIGNATION = "Please provide designation";
CONFIG.ERR_MISSING_PROFILE_IMG = "Please provide profile image.";
CONFIG.ERR_MISSING_PASSBOOK_IMG = "Please provide adhar image.";
CONFIG.ERR_MISSING_ADHAR_IMG = "Please provide adhar image.";
CONFIG.ERR_MISSING_IMG = "Please provide image.";
CONFIG.ERR_NOT_AUTHORISED = "You are not authorized.";

//ADD USER
CONFIG.SUCC_USER_ADDED = "User is added successfully";
CONFIG.ERR_MISSING_USER_ID = "Please provid user id";
CONFIG.SUCC_DETAILS_UPDATED = "Details is updated successfully";

//UPDATE EMPLOYEE
CONFIG.SUCC_EMPLOYEE_UPDATED = "Employee is updated successfully";
CONFIG.SUCC_STATUS = "Status is updated successfully";
CONFIG.ERR_MISSING_IMAGE = "Please provide image url";

//UPDATE USER
CONFIG.SUCC_CLIENT_ADDED = "User is added successfully";
CONFIG.ERR_MISSING_CLIENT_ID = "Please provid client id";
CONFIG.SUCC_DETAILS_UPDATED = "Details is updated successfully";

//CHANGE EMPLOYEE STATUS
CONFIG.ERR_MISSING_EMPLOYEE_ID = "Please provide user id";
CONFIG.ERR_MISSING_STATUS = "Please provide status";
CONFIG.ERR_USER_NOT_FOUND = "User is not exist";

//DELETE USER
CONFIG.SUCC_USER_DELETE = "user is delete successfully"


//VERIFY OTP
CONFIG.ERR_INVALID_OTP = "Your otp is expired or invalid.";
CONFIG.ERR_MISSING_OTP = "Please provide otp.";
CONFIG.ERR_ACCOUNT_NOT_ACTIVE = "Your account will be activated after admin approval.";
CONFIG.SUCC_OTP_VERIFIED = "Otp is verified successfully.";

//ADD DESIGNATION
CONFIG.MISSING_DESIGNATION = "Please provide designation";
CONFIG.SUCC_DESIGNATION_UPDATED = "Designation details is updated successfully";
CONFIG.SUCC_DESIGNATION_ADDED = "Designation details is added successfully";

//DESIGNATIONS
CONFIG.DOCTOR = 1
CONFIG.COUNSLER = 2
CONFIG.PATIENT = 3
