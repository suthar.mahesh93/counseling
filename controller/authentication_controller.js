const AuthenticationService = require('../service/authentication_service');

module.exports = {
    sendOtp,
    verifyOtp
}


async function sendOtp(req, res) {
    AuthenticationService.sendOtp(req, res)
        .then(result => {
            return ReS(res, { message: result }, CONFIG.SUCCESS_CODE);
        })
        .catch(error => {
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}
async function verifyOtp(req, res) {
    AuthenticationService.verifyOtp(req, res)
        .then(result => {
            return ReS(res, { data: result, message: CONFIG.SUCC_OTP_VERIFIED }, CONFIG.SUCCESS_CODE);
        })
        .catch(error => {
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}