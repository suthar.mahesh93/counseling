const DesignationService = require('../service/designation_service');

module.exports = {
    addDesignation,
    getDesignationList,
    updateDesignationById,
    getDesignationById
}
async function addDesignation(req, res) {
    DesignationService.addDesignation(req, res)
        .then(result => {
            return ReS(res, { message: result }, CONFIG.SUCCESS_CODE);
        })
        .catch(error => {
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}

async function getDesignationList(req, res) {
    DesignationService.getDesignationList(req, res)
        .then(result => {
            return ReS(res, { data: result }, CONFIG.SUCCESS_CODE);
        })
        .catch(error => {
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}

async function getDesignationById(req, res) {
    DesignationService.getDesignationById(req, res)
        .then(result => {
            return ReS(res, result, CONFIG.SUCCESS_CODE);
        })
        .catch(error => {
            return ReE(res, error, CONFIG.ERROR_CODE);
        })
}

async function updateDesignationById(req, res) {
    DesignationService.updateDesignationById(req, res)
        .then(result => {
            return ReS(res, { message: result }, CONFIG.SUCCESS_CODE);
        })
        .catch(error => {
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}
