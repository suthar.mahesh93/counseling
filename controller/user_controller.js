const UserService = require('../service/user_service')

module.exports ={
    addUser,
    updateUser,
    changeUserStatus,
    findUser
}

//ADD NEW UESR
async function addUser(req,res){
    UserService.addUser(req,res)
        .then(result =>{
            return ReS(res, { data: result, message: CONFIG.SUCC_USER_ADDED }, CONFIG.SUCCESS_CODE);
        })
        .catch(error =>{
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}

//UPDATE UESR
async function updateUser(req,res){
    UserService.updateUser(req,res)
        .then(result =>{
            return ReS(res, { message: CONFIG.SUCC_DETAILS_UPDATED }, CONFIG.SUCCESS_CODE);
        })
        .catch(error =>{
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}

// CHANGE STATUS USER
async function changeUserStatus(req,res){
    UserService.changeUserStatus(req,res)
        .then(result =>{
            return ReS(res, { message: CONFIG.SUCC_STATUS }, CONFIG.SUCCESS_CODE);
        })
        .catch(error =>{
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}

//USER FINDS
async function findUser(req,res){
    UserService.findUser(req,res)
        .then(result =>{
            return ReS(res, { data: result }, CONFIG.SUCCESS_CODE);
        })
        .catch(error =>{
            return ReE(res, { message: error }, CONFIG.ERROR_CODE);
        })
}