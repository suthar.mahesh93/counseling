const JwtStrategy = require("passport-jwt").Strategy;
const ExtractJwt = require("passport-jwt").ExtractJwt;
const User = require("../models").user;


module.exports = function (passport) {

  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

  opts.secretOrKey = CONFIG.JWT_ENCRYPTION;
  passport.use(
    new JwtStrategy(opts, async function (jwt_payload, done) {

      if (!jwt_payload.userId)
        return done(null, false);

      let err, result;
      [err, result] = await to(
        User.findOne({ where: { userId: jwt_payload.userId } })
      );
      if (err) return done(err, false);
      if (result)
        return done(null, result);
      else
        return done(null, false);
    })
  );
};
