module.exports = (sequelize, Sequelize) => {
    const Designation = sequelize.define(
        "designation",
        {
            designationId: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true
            },
            designation: {
                type: Sequelize.STRING,
                allowNull: false,
                unique: true
            },
            status: { type: Sequelize.INTEGER, defaultValue: CONFIG.RECORD_ACTIVE },

        },
        {
            indexes: [
                {
                    name: "designation_id_index",
                    method: "BTREE",
                    fields: ["designationId"]
                }
            ]
        }
    );
    return Designation;
};
/**
'1','Doctor'
'2','Counsler'
'3','Patient'
*/