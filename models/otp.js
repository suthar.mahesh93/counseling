module.exports = (sequelize,Sequelize) => {
    var Otp = sequelize.define(
        'otp',
        {
            otpId: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true
            },
            otp: Sequelize.INTEGER,
            otpType: Sequelize.INTEGER, //1.Phone
            phone: { type: Sequelize.STRING, defaultValue: "" },
        },
        {
            indexes: [
                {
                    name: "otp_id_index",
                    method: "BTREE",
                    fields: ["otpId"]
                },
                {
                    name: "otp_index",
                    method: "BTREE",
                    fields: ["otpType"]
                },
                {
                    name: "phone_index",
                    method: "BTREE",
                    fields: ["phone"]
                }
            ]
        }
    )
    return Otp;
};