const jwt = require("jsonwebtoken");

module.exports = (sequelize,Sequelize) => {
    var User = sequelize.define(
        'user',
        {
            userId: {
                primaryKey: true,
                type: Sequelize.INTEGER,
                autoIncrement: true
            },
            firstName: {type: Sequelize.STRING ,defaultValue:""}, 
            middleName: {type: Sequelize.STRING, defaultValue: ""},
            lastName: {type: Sequelize.STRING, defaultValue:""},
            phone: {
                type: Sequelize.STRING,
                allowNull:false,
                unique:true
            },
            email: {type: Sequelize.STRING},
            gender: Sequelize.ENUM({
                values: ['Male', 'Female']
            }),
            age: {type: Sequelize.INTEGER, defaultValue:0},
            address: {type: Sequelize.TEXT, defaultValue:""},
            password: {type: Sequelize.STRING,defaultValue:""},
            status: { type: Sequelize.INTEGER,defaultValue:CONFIG.USER_REGISTERED},
            createdBy: { type: Sequelize.STRING, defaultValue: "" },
            image:{type: Sequelize.STRING, defaultValue:""},
            token: { type: Sequelize.STRING, defaultValue: "" },
            lastLogin: { type: Sequelize.DATE, defaultValue: Sequelize.NOW }
        }
    );
    User.associate = function (models){
        User.belongsTo(models.designation, {
            foreignKey: "designationId",
            onDelete: "cascade"
        });
    };
    User.prototype.getJWT = function () {
        return (
            "Bearer " +
            jwt.sign(
                { userId: this.userId, type: "User" },
                CONFIG.JWT_ENCRYPTION
            )
        );
    };

    return User;
};