var express = require("express");
var router = express.Router();
const passport = require("passport");
require("./../../middleware/passport")(passport);
const UserController = require('../../controller/user_controller');
const AuthenticationController = require('../../controller/authentication_controller');
const DesignationController = require('../../controller/designation_controller');

router.get('/', function (req, res, next) {
    res.send('Welcome to counseling v1 APIs...');
});

router.post('/', function (req, res, next) {
    res.send('Welcome to counseling v1 APIs...');
});

//=====================================UESR
router.post('/addUser',
    passport.authenticate("jwt", { session: false }),
    UserController.addUser
)
router.post('/updateUser',
    UserController.updateUser
)
router.post('/changeUserStatus',
    UserController.changeUserStatus
)
router.post('/findUser',
    UserController.findUser
)

//===============================AUTHENTICATION
router.post('/sendOtp', AuthenticationController.sendOtp)
router.post('/verifyOtp', AuthenticationController.verifyOtp)

//==============================DESIGNATION   
router.post('/addDesignation',
    DesignationController.addDesignation)

router.put('/updateDesignationById',
    DesignationController.updateDesignationById)

router.get('/getDesignationList',
    DesignationController.getDesignationList)

router.get('/getDesignationById/:designationId',
    DesignationController.getDesignationById)

module.exports = router;