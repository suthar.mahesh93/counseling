const User = require("../models").user;
const Designation = require("../models").designation;
const Otp = require("../models").otp;
const Sequelize = require('sequelize');
const Op = Sequelize.Op;

module.exports = {
    sendOtp,
    verifyOtp
};

async function sendOtp(req, res) {
    return new Promise(async function (resolve, reject) {
        var body = req.body;
        if (!body.phone)
            return reject(CONFIG.ERR_MISSING_PHONE);

        var otp = 1111;
        var [errOtp, resultOtp] = await to(Otp.create({
            otp: otp,
            phone: body.phone,
            otpType: 1
        }));

        if (errOtp || !resultOtp)
            return reject(CONFIG.ERR_SERVER)
        else
            return resolve(`Otp sent successfully on ${body.phone}`);

    });
}
async function verifyOtp(req, res) {
    return new Promise(async function (resolve, reject) {
        var body = req.body;

        if (!body.phone)
            return reject(CONFIG.ERR_MISSING_PHONE);
        else if (!body.otp)
            return reject(CONFIG.ERR_MISSING_OTP);

        var [err, userData] = await to(
            User.findOne({
                where: {
                    phone: body.phone,
                    status: CONFIG.USER_ACTIVE,
                    designationId: {
                        [Op.in]: [
                            CONFIG.DOCTOR,
                            CONFIG.COUNSLER
                        ]
                    }
                },
                include: [{ model: Designation }]

            }));
        var [errOtp, resultOtp] = await to(Otp.findAll({
            limit: 1,
            where: {
                phone: body.phone,
                otpType: 1
            },
            order: [['createdAt', 'DESC']]
        }));

        if (errOtp)
            return reject(CONFIG.ERR_SERVER)
        else if (!resultOtp || resultOtp.length == 0)
            return resolve(CONFIG.ERR_INVALID_OTP);    

        var seconds = (new Date().getTime() - new Date(resultOtp[0].dataValues.createdAt).getTime()) / 1000;

        if (seconds > CONFIG.OTP_EXP_SECONDS)
            return reject(CONFIG.ERR_INVALID_OTP);

        if (!userData) // create new one
        {

            return reject(CONFIG.ERR_NOT_AUTHORISED)
            // var [err, result] = await to(Employee.create({
            //     phone: body.phone
            // }));

            // if (err || !result)
            //     return reject(CONFIG.ERR_SERVER)

            // employee = result;
        }

        if (userData.status != CONFIG.USER_ACTIVE)
            return reject(CONFIG.ERR_ACCOUNT_NOT_ACTIVE)

        var token = userData.getJWT();

        var user = {
            token: token,
            lastLogin: new Date(),
        }
        var [err, result] = await to(userData.update(user));

        if (err)
            return reject(CONFIG.ERR_SERVER)

        return resolve(result);
    });
}