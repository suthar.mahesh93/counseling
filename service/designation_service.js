const Designation = require("../models").designation;

module.exports = {
    addDesignation,
    getDesignationList,
    getDesignationById,
    updateDesignationById
};
//ADD NEW DESIGNATION
async function addDesignation(req, res) {
    return new Promise(async function (resolve, reject) {
        var body = req.body;
        if (!body.designation)
            return reject(CONFIG.MISSING_DESIGNATION);

        Designation.create(req.body)
            .then(result => {
                return resolve(CONFIG.SUCC_DESIGNATION_ADDED);
            })
            .catch(error => {
                return reject(error);
            });
    });
}
//GET ALL DESIGNATION
async function getDesignationList(req, res) {
    return new Promise(async function (resolve, reject) {
        var limit, page, offset, body;
        body = req.body;
        // limit = parseInt(body.limit == undefined ? 50 : body.limit); // number of records per page
        // page = parseInt(body.page == undefined ? 1 : body.page); //page
        // offset = (limit * (page - 1));

        Designation.findAll({
            order: [
                ['designation', 'ASC']
            ],
            //limit: limit,
            //offset: offset,
        })
            .then(result => {
                return resolve(result);
            })
            .catch(error => {
                return reject(error);
            });
    });
}

//GET DESIGNATION DETAILS BY ID
async function getDesignationById(req, res) {
    return new Promise(async function (resolve, reject) {
        var param;
        param = req.params;
        Designation.findOne({
            where: { designationId: param.designationId },
            status: CONFIG.RECORD_ACTIVE,
        })
            .then(result => {
                return resolve(result);
            })
            .catch(error => {
                return reject(error);
            });
    });
}

//UPDATE DESIGNATION DETAILS BY ID
async function updateDesignationById(req, res) {
    return new Promise(async function (resolve, reject) {
        var body;
        body = req.body;
        Designation.update({
            designation: body.designation
        }, { where: { designationId: body.designationId } })
            .then(result => {
                return resolve(CONFIG.SUCC_DESIGNATION_UPDATED);
            })
            .catch(error => {
                return reject(error);
            });
    });
}




