const { Sequelize } = require("sequelize");
const { sequelize } = require("sequelize");
const Op = Sequelize.Op;
const User = require("../models").user;

module.exports ={
    addUser,
    updateUser,
    changeUserStatus,
    findUser
}

//REGISTER NEW USER
async function addUser(req,res){
    return new Promise(async function (resolve, reject){
        var body = req.body;
        console.log(body);
        if (!body.firstName)
            return reject(CONFIG.ERR_MISSING_FNAME);
        else if (!body.middleName)
            return reject(CONFIG.ERR_MISSING_EMAIL);
        else if (!body.lastName)
            return reject(CONFIG.ERR_MISSING_LNAME);
        else if (!body.phone)
            return reject(CONFIG.ERR_MISSING_PHONE);
        else if (!body.gender)
            return reject(CONFIG.ERR_MISSING_GENDER);
        else if (!body.age)
            return reject(CONFIG.ERR_MISSING_AGE);
        else if (!body.address)
            return reject(CONFIG.ERR_MISSING_ADDRESS);
        else if (!body.password)
            return reject(CONFIG.ERR_MISSING_ADDRESS);
        // body.createdBy = req.user.firstName  

        var [err, user] = await to(
            User.findOne({
                where: {
                    [Op.or]:[{phone: body.phone},{email: body.email}]
                }
            }));
        if (err) return reject(CONFIG.ERR_SERVER);

        else if (user) return reject(CONFIG.ERR_USER_EXIST)

        User.create(body)
            .then(async result=>{
                return resolve(result)
            })
            .catch(error =>{
                return reject(error)
            })
    })
}

// TO UPDATE USER PROFILE
async function updateUser(req,res) {
    return new Promise(async function (resolve, reject){
        var body = req.body;
        
        if(!body.userId)
            return reject(CONFIG.ERR_MISSING_USER_ID)
        else if (!body.firstName)
            return reject(CONFIG.ERR_MISSING_FNAME);
        else if (!body.middleName)
            return reject(CONFIG.ERR_MISSING_EMAIL);
        else if (!body.lastName)
            return reject(CONFIG.ERR_MISSING_LNAME);
        else if (!body.phone)
            return reject(CONFIG.ERR_MISSING_PHONE);
        else if (!body.gender)
            return reject(CONFIG.ERR_MISSING_GENDER);
        else if (!body.age)
            return reject(CONFIG.ERR_MISSING_AGE);
        else if (!body.address)
            return reject(CONFIG.ERR_MISSING_ADDRESS);
        console.log(body);
        var [error, user] = await to(User.findOne({ where: { userId: body.userId }, attributes: ["userId"] }));

        if (error)
            return reject(CONFIG.ERR_SERVER);
        else if (!user)
            return reject(CONFIG.ERR_USER_NOT_FOUND);

        user.update(body)
            .then(async result => {
                return resolve(result);
            })
            .catch(error => {
                return reject(error);
            });
    })
}

//TO DELETE USER 
async function changeUserStatus(req,res) {
    return new Promise(async function (resolve, reject){
        var body = req.body;
        
        if(!body.userId)
            return reject(CONFIG.ERR_MISSING_USER_ID)
        else if (!body.status)
            return reject(CONFIG.ERR_MISSING_STATUS);
        console.log(body);

        var [error, user] = await to(User.findOne({ where: { userId: body.userId } }));

        if (error)
            return reject(CONFIG.ERR_SERVER);
        else if (!user)
            return reject(CONFIG.ERR_USER_NOT_FOUND);
        user.update({ status: body.status })
            .then(result => {
                return resolve(result)
            })
            .catch(err => {
                console.log(err);
                return reject(err)
        })
    })
}

//TO FIND ONE USER
async function findUser(req,res){
    return new Promise(async function (resolve, reject){
        var body = req.body;
        if(!body.userId)
            return reject(CONFIG.ERR_MISSING_USER_ID)
        console.log(body);

        var [error, user] = await to(User.findOne({ where: { userId: body.userId } }));

        if (error)
            return reject(CONFIG.ERR_SERVER);
        else if (!user)
            return reject(CONFIG.ERR_USER_NOT_FOUND);
        return resolve(user);
    })
}